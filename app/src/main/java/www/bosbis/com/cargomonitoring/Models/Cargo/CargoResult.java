
package www.bosbis.com.cargomonitoring.Models.Cargo;

import java.util.List;
import javax.annotation.Generated;

import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class CargoResult {

    @SerializedName("num_found")
    private Long numFound;

    @SerializedName("rows")
    private List<CargoRow> cargoRows;


    public Long getNumFound() {
        return numFound;
    }

    public void setNumFound(Long numFound) {

        this.numFound = numFound;
    }

    public List<CargoRow> getCargoRows() {

        return cargoRows;
    }

    public void setCargoRows(List<CargoRow> cargoRows) {
        this.cargoRows = cargoRows;
    }



}

