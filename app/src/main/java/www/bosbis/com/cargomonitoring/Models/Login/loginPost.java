
package www.bosbis.com.cargomonitoring.Models.Login;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;


@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class loginPost extends loginResponse {


    @SerializedName("password")
    private String mPassword;

    @SerializedName("username")
    private String mUsername;

    public loginPost(String username, String password) {
        this.mPassword = password;
        this.mUsername = username;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public String getUsername() {
        return mUsername;
    }

    public void setUsername(String username) {
        mUsername = username;
    }

}

