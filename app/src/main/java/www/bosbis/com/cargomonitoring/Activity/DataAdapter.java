package www.bosbis.com.cargomonitoring.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

import www.bosbis.com.cargomonitoring.Models.Cargo.CargoRow;
import www.bosbis.com.cargomonitoring.R;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {

    private ArrayList<CargoRow> data = new ArrayList<>();


    @NonNull
    @Override
    public DataAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent , int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_cargo,  parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewholder, final int position){
        viewholder.tv1.setText(data.get(position).getCtpName());
        viewholder.tv2.setText(data.get(position).getCtConote());
        viewholder.tv3.setText(data.get(position).getCrFrom());
        viewholder.tv4.setText(data.get(position).getCrTo());
        viewholder.tv5.setText(data.get(position).getPoBrand());
        viewholder.tv6.setText(data.get(position).getDepartureDate());
        viewholder.tv7.setText(String.format("Rp.%s", data.get(position).getCtTotalPrice()));
        viewholder.itemList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = v.getContext();
                Intent intent = new Intent(v.getContext(), DetailsStatus.class);
                intent.putExtra("name", data.get(position).getCtpName());
                intent.putExtra("note", data.get(position).getCtConote());
                intent.putExtra("from", data.get(position).getCrFrom());
                intent.putExtra("pobrand", data.get(position).getPoBrand());
                intent.putExtra("departure", data.get(position).getDepartureDate());
                intent.putExtra("to", data.get(position).getCrTo());
                intent.putExtra("price", data.get(position).getCtTotalPrice());
                intent.putExtra("ct_status",data.get(0).getAvailableChangeStatus().get(0).getCosName());
                context.startActivity(intent);

            }
            });
    }

        @Override
    public int getItemCount() {
             if (data == null)
            return 0;
        else
            return  data.size();
    }

    void clearItems() {
        this.data.clear();
        notifyDataSetChanged();
    }

    void addItems(ArrayList<CargoRow> data) {
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    class ViewHolder extends  RecyclerView.ViewHolder {
        TextView tv1, tv2, tv3, tv4, tv5, tv6,tv7,tv8;
        CardView itemList;
        Spinner spinner;
         ViewHolder(View view){
            super(view);
            tv1  =view.findViewById(R.id.tv1);
            tv2 =view.findViewById(R.id.tv2);
            tv3 =view.findViewById(R.id.tv3);
            tv4 =view.findViewById(R.id.tv4);
            tv5 =view.findViewById(R.id.tv5);
            tv6 =view.findViewById(R.id.tv6);
            tv7= view.findViewById(R.id.tv7);
            itemList = view.findViewById(R.id.cargoview);
            spinner =view.findViewById(R.id.spinner);



        }
    }
}
