package www.bosbis.com.cargomonitoring.Api;

import android.app.Application;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import www.bosbis.com.cargomonitoring.Api.ApiInterface;


public class MainAplication  extends Application {

    public  static final String BASE_URL ="https://apicore.bisnetwork.id/";
    public static ApiInterface service;

    @Override
    public void onCreate(){
        super.onCreate();
    }
    public static ApiInterface getApiClient() {
        if (service == null) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            httpClient.addInterceptor(logging);
            Retrofit retrofit =new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.build())
                    .build();
            service = retrofit.create(ApiInterface.class);
        }

        return service;

    }

}
