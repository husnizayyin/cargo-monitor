
package www.bosbis.com.cargomonitoring.Models.StatusCargo;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
public class getStatus {

    @SerializedName("code")
    private Long mCode;
    @SerializedName("error")
    private Boolean mError;
    @SerializedName("message")
    private String mMessage;
    @SerializedName("result")
    private statusResult mStatusResult;

    public Long getCode() {
        return mCode;
    }

    public void setCode(Long code) {
        mCode = code;
    }

    public Boolean getError() {
        return mError;
    }

    public void setError(Boolean error) {
        mError = error;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public statusResult getResult() {
        return mStatusResult;
    }

    public void setResult(statusResult statusResult) {
        mStatusResult = statusResult;
    }

}
