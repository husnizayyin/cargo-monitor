
package www.bosbis.com.cargomonitoring.Models.Cargo;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class GetCargo {

    @Expose
    private Long code;
    @Expose
    private Boolean error;
    @Expose
    private String message;
    @Expose
    private CargoResult cargoResult;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public CargoResult getCargoResult() {
        return cargoResult;
    }

    public void setCargoResult(CargoResult cargoResult) {
        this.cargoResult = cargoResult;
    }

}
