package www.bosbis.com.cargomonitoring.Models.Cargo;

import com.google.gson.annotations.SerializedName;

public class CargoResultResponse {

    @SerializedName("result")
    public  CargoResult cargoResult;

    public CargoResult getCargoResult() { return cargoResult; }

    public void setCargoResult(CargoResult cargoResult) {
        this.cargoResult = cargoResult;
    }
}
