
package www.bosbis.com.cargomonitoring.Models.Login;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class loginResponse {

    @SerializedName("code")
    private Long mCode;
    @SerializedName("error")
    private Boolean mError;
    @SerializedName("message")
    private String mMessage;
    @SerializedName("result")
    private loginResponse mLoginResponse;
    @SerializedName("username")
    private String mUsername;
    @SerializedName("password")
    private  String mPassword;

    public loginResponse(String username, String password) {
        mUsername=username;
        mPassword=password;

    }

    public loginResponse() {
    }


    @SerializedName("body")

    public Long getCode() {
        return mCode;
    }

    public void setCode(Long code) {
        mCode = code;
    }

    public Boolean getError() {
        return mError;
    }

    public void setError(Boolean error) {
        mError = error;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public loginResponse getResult() {
        return mLoginResponse;
    }

    public void setResult(loginResponse loginResponse) {
        mLoginResponse = loginResponse;
    }

    public String getUsername() {
        return mUsername;
    }

    public void setUsername(String username) {
        mUsername = username;
    }

}
