
package www.bosbis.com.cargomonitoring.Models.Cargo;

import javax.annotation.Generated;

import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class AvailableChangeStatus {

    @SerializedName("cos_id")
    private String cosId;

    @SerializedName("cos_name")
    private String cosName;

    public String getCosId() {

        return cosId;
    }

    public void setCosId(String cosId) {

        this.cosId = cosId;
    }

    public String getCosName() {

        return cosName;
    }

    public void setCosName(String cosName) {

        this.cosName = cosName;
    }

}
