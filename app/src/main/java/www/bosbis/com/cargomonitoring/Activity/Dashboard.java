package www.bosbis.com.cargomonitoring.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.chip.Chip;
import android.support.design.chip.ChipGroup;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;
import java.util.ArrayList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import www.bosbis.com.cargomonitoring.Models.Cargo.CargoResultResponse;
import www.bosbis.com.cargomonitoring.Models.Cargo.CargoRow;
import www.bosbis.com.cargomonitoring.Models.StatusCargo.getStatus;
import www.bosbis.com.cargomonitoring.Models.StatusCargo.statusRow;
import www.bosbis.com.cargomonitoring.R;
import static www.bosbis.com.cargomonitoring.Api.MainAplication.service;

public class Dashboard extends AppCompatActivity {
    ChipGroup chipGroup;
    RecyclerView recyclerView;
    DataAdapter mAdapater;

    private ArrayList<CargoRow> data ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard_layout);
        chipGroup = findViewById(R.id.chipGroup);
        chipGroup.isClickable();
        chipGroup.isFocusable();
        recyclerView = findViewById(R.id.rv1);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(Dashboard.this);
        recyclerView.setLayoutManager(layoutManager);
        mAdapater = new DataAdapter();
        recyclerView.setAdapter(mAdapater);
        getResponse();
    }

    private void getResponse() {
        Call<getStatus> call = service.createStatus();
        call.enqueue(new Callback<getStatus>() {
            @Override
            public void onResponse(@NonNull Call<getStatus> call, @NonNull final Response<getStatus> response) {
                for (final statusRow statusRow : response.body().getResult().getRows()) {
                    View v = View.inflate(getBaseContext(), R.layout.item_chip, null);
                    ((Chip) v.findViewById(R.id.view_chip)).setChipText(statusRow.getStatusName());
                    ((Chip) v.findViewById(R.id.view_chip)).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Cargo(statusRow.getStatusId() + "");
                            if(statusRow.getStatusId()==null){
                                Toast.makeText(Dashboard.this, response.body().getResult().getRows().get(0).getStatusName(),Toast.LENGTH_SHORT).show();
                            }

                        }
                    });
                    chipGroup.addView(v);
                }
                Cargo(response.body().getResult().getRows().get(0).getStatusId() + "");
                chipGroup.isSingleSelection();
            }

            @Override
            public void onFailure(Call<getStatus> call, Throwable t) {
                Toast.makeText(Dashboard.this, "Something wrong please call customer service", Toast.LENGTH_SHORT).show();
                return;
            }
        });
    }

    private void Cargo(String status){
        mAdapater.clearItems();
        Call<CargoResultResponse> call = service.readCargo(status);
        call.enqueue(new Callback<CargoResultResponse>() {
            @Override
            public void onResponse(Call<CargoResultResponse> call, Response<CargoResultResponse> response) {
                if (response.body().getCargoResult() == null || response.body().getCargoResult().getCargoRows().size()==0) {
                    Toast.makeText(Dashboard.this, "Data tidak ditemukan", Toast.LENGTH_SHORT).show();
                    return;
                }

                ArrayList<CargoRow> list = new ArrayList<>();
                for (CargoRow item : response.body().getCargoResult().getCargoRows()) {
                    list.add(item);
                }

                mAdapater.addItems(list);
            }

            @Override
            public void onFailure(Call<CargoResultResponse> call, Throwable t) {
                Toast.makeText(Dashboard.this, "Something wrong please call customer service", Toast.LENGTH_SHORT).show();
                return;

            }
        });
    }

}