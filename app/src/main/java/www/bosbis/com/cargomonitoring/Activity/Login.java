package www.bosbis.com.cargomonitoring.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.HEAD;
import www.bosbis.com.cargomonitoring.Api.MainAplication;
import www.bosbis.com.cargomonitoring.Models.Login.loginResponse;
import www.bosbis.com.cargomonitoring.R;

public class Login extends AppCompatActivity  implements View.OnClickListener {

    Button btlogin;
    EditText etUsername;
    EditText etPw;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);
        btlogin = findViewById(R.id.btlogin);
        etUsername = findViewById(R.id.etUsername);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            etUsername.setImportantForAutofill(View.IMPORTANT_FOR_AUTOFILL_AUTO);
        }
        etPw = findViewById(R.id.etPw);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            etPw.setImportantForAutofill(View.IMPORTANT_FOR_AUTOFILL_AUTO);
        }btlogin.setOnClickListener(this);
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btlogin:
                final ProgressDialog progressDialog = new ProgressDialog(Login.this, R.style.ThemeOverlay_AppCompat_Dialog);
                progressDialog.setIndeterminate(true);
                progressDialog.setMessage("Loading");
                progressDialog.setCancelable(true);
                progressDialog.show();
                final loginResponse loginresponse= new loginResponse(etUsername.getText().toString(), etPw.getText().toString());
                MainAplication.getApiClient().createUser(loginresponse).enqueue(
                        new Callback<loginResponse>() {
                    @Override
                    public void onResponse(Call<loginResponse> call, Response<loginResponse> response) {
                        loginResponse responseLogin = response.body();
                        if (response.isSuccessful()) {
                            loginResponse loginResponse = response.body();
                            Toast.makeText(Login.this,
                                    responseLogin.getMessage(),Toast.LENGTH_LONG).show();
                            Intent i =new Intent (Login.this, Dashboard.class);
                            startActivity(i);
                        }
                    }

                    @Override
                    public void onFailure(Call<loginResponse> call, Throwable t) {
                        Toast.makeText(Login.this, loginresponse.getMessage()
                                , Toast.LENGTH_LONG).show();

                    }
                });

                break ;

        }

    }
}





