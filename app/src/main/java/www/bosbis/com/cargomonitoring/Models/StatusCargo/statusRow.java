
package www.bosbis.com.cargomonitoring.Models.StatusCargo;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class statusRow {

    @SerializedName("status_id")
    private Long mStatusId;
    @SerializedName("status_name")
    private String mStatusName;

    public Long getStatusId() {
        return mStatusId;
    }

    public void setStatusId(Long statusId) {
        mStatusId = statusId;
    }

    public String getStatusName() {
        return mStatusName;
    }

    public void setStatusName(String statusName) {
        mStatusName = statusName;
    }

}
