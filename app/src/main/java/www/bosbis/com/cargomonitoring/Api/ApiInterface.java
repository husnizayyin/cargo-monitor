package www.bosbis.com.cargomonitoring.Api;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import www.bosbis.com.cargomonitoring.Models.Cargo.CargoResultResponse;
import www.bosbis.com.cargomonitoring.Models.Cargo.CargoRowRespone;
import www.bosbis.com.cargomonitoring.Models.StatusCargo.getStatus;
import www.bosbis.com.cargomonitoring.Models.Login.loginResponse;

public interface ApiInterface {

    @Headers({
            "Content-Type:application/json"
    })
    @POST("/cargo/monitor/login")
    Call<loginResponse> createUser(@Body loginResponse loginresponse);

    @GET("/cargo/monitor/cargo/status")
    Call<getStatus> createStatus();

     @GET("/cargo/monitor/cargo/view")
    Call<CargoResultResponse> readCargo(@Query("ct_status") String status);

     @GET("/cargo/monitor/cargo/view")
    Call<CargoRowRespone> changeStatus(@Query("ct_status") String ganti);
        //();



}

