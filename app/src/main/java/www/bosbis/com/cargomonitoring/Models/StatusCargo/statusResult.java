
package www.bosbis.com.cargomonitoring.Models.StatusCargo;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class statusResult  {

    @SerializedName("num_found")
    private Long mNumFound;
    @SerializedName("rows")
    private List<statusRow> mStatusRows;

    public Long getNumFound() {
        return mNumFound;
    }

    public void setNumFound(Long numFound) {
        mNumFound = numFound;
    }

    public List<statusRow> getRows() {
        return mStatusRows;
    }

    public void setRows(List<statusRow> statusRows) {
        mStatusRows = statusRows;
    }

}
