
package www.bosbis.com.cargomonitoring.Models.Cargo;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

import java.util.List;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class CargoRow {

    @SerializedName("cr_from")
    private String crFrom;
    @SerializedName("cr_to")
    private String crTo;
    @SerializedName("ct_conote")
    private String ctConote;
    @SerializedName("ct_id")
    private Long ctId;
    @SerializedName("ct_item_unit_amount")
    private String ctItemUnitAmount;
    @SerializedName("ct_total_price")
    private String ctTotalPrice;
    @SerializedName("ctp_name")
    private String ctpName;
    @SerializedName("departure_date")
    private String departureDate;
    @SerializedName("po_brand")
    private String poBrand;
    @SerializedName("available_change_status")
    private List<AvailableChangeStatus> availableChangeStatus;


    public String getCrFrom() {
        return crFrom;
    }

    public void setCrFrom(String crFrom) {
        this.crFrom = crFrom;
    }

    public String getCrTo() {
        return crTo;
    }

    public void setCrTo(String crTo) {
        this.crTo = crTo;
    }

    public String getCtConote() {
        return ctConote;
    }

    public void setCtConote(String ctConote) {
        this.ctConote = ctConote;
    }

    public Long getCtId()
    {
        return ctId;
    }

    public void setCtId(Long ctId) { this.ctId = ctId; }

    public String getCtItemUnitAmount() { return ctItemUnitAmount; }

    public void setCtItemUnitAmount(String ctItemUnitAmount) { this.ctItemUnitAmount = ctItemUnitAmount; }

    public String getCtTotalPrice() { return ctTotalPrice; }

    public void setCtTotalPrice(String ctTotalPrice)
    {
        this.ctTotalPrice = ctTotalPrice;
    }

    public String getCtpName() { return ctpName; }

    public void setCtpName(String ctpName)

    {
        this.ctpName = ctpName;
    }

    public String getDepartureDate()

    {
        return departureDate;
    }

    public void setDepartureDate(String departureDate)
    {
        this.departureDate = departureDate;
    }

    public String getPoBrand()

    {
        return poBrand;
    }

    public void setPoBrand(String poBrand) { this.poBrand = poBrand; }
    public List<AvailableChangeStatus> getAvailableChangeStatus() { return availableChangeStatus; }

    public void setAvailableChangeStatus(List<AvailableChangeStatus> availableChangeStatus) {this.availableChangeStatus = availableChangeStatus; }




}
