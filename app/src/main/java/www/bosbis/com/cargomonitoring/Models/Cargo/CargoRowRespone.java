package www.bosbis.com.cargomonitoring.Models.Cargo;

import com.google.gson.annotations.SerializedName;

public class CargoRowRespone {
    @SerializedName("result")
    public  CargoResult getResult;

    @SerializedName("rows")
    public  CargoRow getCargoRows;

    public CargoResult getGetResult() {
        return getResult;
    }

    public void setGetResult(CargoResult getResult) {
        this.getResult = getResult;
    }

    public CargoRow getCargoRows() { return getCargoRows; }

    public void setCargoRows(CargoRow getCargoRows) { this.getCargoRows = getCargoRows; }
}
