package www.bosbis.com.cargomonitoring.Activity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import www.bosbis.com.cargomonitoring.Models.Cargo.CargoRow;
import www.bosbis.com.cargomonitoring.R;

public class DetailsStatus extends AppCompatActivity {
    TextView tv1, tv2, tv3, tv4,tv5,tv6,tv7,tv8;
    TextInputEditText ChoseDate;
    TextInputEditText ChoseTime;
    Spinner spinner;
    Context mContext;
    private ArrayList<CargoRow> data ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details_status);
        tv1 = findViewById(R.id.tv_ctpName);
        tv2 = findViewById(R.id.tv_ctNote);
        tv3 = findViewById(R.id.tv_ctPoBrand);
        tv4  = findViewById(R.id.tv_ctFrom);
        tv5 = findViewById(R.id.tv_ctDepatureDate);
        tv6 =  findViewById(R.id.tv_ctItem);
        tv8= findViewById(R.id.tv_ctTo);
        tv7= findViewById(R.id.tv_ctPrice);


        tv2.setText(getIntent().getStringExtra("note"));
        tv1.setText(getIntent().getStringExtra("name"));
        tv3.setText(getIntent().getStringExtra("pobrand"));
        tv4.setText(getIntent().getStringExtra("from"));
        tv5.setText(getIntent().getStringExtra("departure"));
        tv6.setText(getIntent().getStringExtra("item"));
        tv7.setText(String.format("Rp.%s", getIntent().getStringExtra("price")));
        tv8.setText(getIntent().getStringExtra("to"));


        ChoseDate = findViewById(R.id.datePicker);
        ChoseTime = findViewById(R.id.timePicker);
        mContext = this;
        final Calendar calendar = Calendar.getInstance();


        final DatePickerDialog .OnDateSetListener getDate = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker  view, int year, int month, int dayOfMonth) {
                calendar.set(Calendar.YEAR,year);
                calendar.set(Calendar.MONTH,month);
                calendar.set(Calendar.DAY_OF_MONTH,dayOfMonth);
                updatelabel();

            }

            private void updatelabel() {
                String format = "dd MMM yyyy";
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, Locale.US);
                ChoseDate.setText(simpleDateFormat.format(calendar.getTime()));

            }
        };

        ChoseDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               new DatePickerDialog(DetailsStatus.this,  getDate,calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                       calendar.get(Calendar.DAY_OF_MONTH)).show();

            }
        });
//time
        ChoseTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final TimePickerDialog timePickerDialog = new TimePickerDialog(DetailsStatus.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        calendar.set(Calendar.HOUR_OF_DAY,hourOfDay);
                        calendar.set(Calendar.MINUTE,minute);
                        ChoseTime.setText(String.format("%d:%d", hourOfDay, minute));

                    }
                },
                        Calendar.getInstance().get(Calendar.HOUR_OF_DAY),
                        Calendar.getInstance().get(Calendar.MINUTE),
                        true);
                timePickerDialog.show();


            }
        });
            spinner=findViewById(R.id.spinner);
            List<String> data2 =new ArrayList<>();
            data2.add(getIntent().getStringExtra("ct_status"));
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, data2);
            spinner.setAdapter(dataAdapter);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    String selectedStatus = parent.getSelectedItem().toString();
                    Toast.makeText( mContext, "status dipilih"+  selectedStatus, Toast.LENGTH_SHORT ).show();

                }
                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
        });
        }


    }


